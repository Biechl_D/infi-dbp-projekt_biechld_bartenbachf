import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;

public class AuswahlGUI {

	private JFrame frame;
	Polarice p1 = new Polarice();
	PieChart pieC = new PieChart();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AuswahlGUI window = new AuswahlGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the application.
	 */
	public AuswahlGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 432, 253);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblPolarice = new JLabel("Polareis-Schmelze");
		lblPolarice.setBounds(31, 30, 129, 24);
		panel.add(lblPolarice);
		
		JButton btnShow = new JButton("Show");
		btnShow.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
				try {
					p1.createDataset();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}}
		});
		btnShow.setBounds(234, 30, 65, 25);
		panel.add(btnShow);
		JLabel lblTop = new JLabel("Top 10+/- CO2-E. Laender");
		lblTop.setBounds(31, 81, 140, 16);
		panel.add(lblTop);
		
		JButton button = new JButton("Show");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				ArrayList<CountryData> countrydata=null;
				ArrayList<CountryData> countrylowestdata=null;
				try {
					countrydata = DBmanager.getInstance().CountryHighestEmissionData();
					countrylowestdata = DBmanager.getInstance().CountryLowestEmissionData();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println("Highest: ");
				for(int i = 0; i < countrydata.size(); i++) {
					System.out.println("Ort: " + countrydata.get(i).getName()+ "  Emissionsmenge: "+ countrydata.get(i).getEmissionAmount());
				}
				
				System.out.println("Lowest: ");
				for(int i = 0; i < countrylowestdata.size(); i++) {
					System.out.println("Ort: " + countrylowestdata.get(i).getName()+ "  Emissionsmenge: "+ countrylowestdata.get(i).getEmissionAmount());
				}
				
				
				JFrame frame = new JFrame("Listung");
				JPanel panel = new JPanel();
				
				String[] columnNames = {"Highest-Countries","Amount","Lowest-Countries","Amount"};
	
				DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0);
				JTable table = new JTable(tableModel);
				tableModel.addRow(columnNames);
				for(int i = 0; i < countrydata.size(); i++) {
					//label1.setText(countrydata.get(i).getName()+""+countrydata.get(i).getEmissionAmount());
					//panel.add(label1);
					Object[] data= {countrydata.get(i).getName(), countrydata.get(i).getEmissionAmount(),
							countrylowestdata.get(i).getName(), countrylowestdata.get(i).getEmissionAmount()};
					tableModel.addRow(data);
				}
				panel.add(table);
				
				frame.getContentPane().add(panel);
				frame.pack();
				frame.setVisible(true);			
			}
		});
		button.setBounds(234, 77, 65, 25);
		panel.add(button);
		
		JLabel lblLaenderpiechart = new JLabel("Laender-PieChart");
		lblLaenderpiechart.setBounds(31, 137, 129, 16);
		panel.add(lblLaenderpiechart);
		
		JButton btnShow_1 = new JButton("Show");
		btnShow_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					pieC.createDataset();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}}
		});
		btnShow_1.setBounds(234, 133, 65, 25);
		panel.add(btnShow_1);
	}
}
