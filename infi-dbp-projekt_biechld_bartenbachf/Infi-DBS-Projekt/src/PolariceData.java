
public class PolariceData {

	private int jahre;
	private double extent;
	
	public PolariceData(int jahre, double extent) {
		super();
		this.jahre = jahre;
		this.extent = extent;
	}

	public int getJahre() {
		return jahre;
	}

	public void setJahre(int jahre) {
		this.jahre = jahre;
	}

	public double getExtent() {
		return extent;
	}

	public void setExtent(double extent) {
		this.extent = extent;
	}
	
	
}
