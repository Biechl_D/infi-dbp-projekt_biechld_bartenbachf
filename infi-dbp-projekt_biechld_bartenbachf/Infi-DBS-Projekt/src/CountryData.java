
public class CountryData {

	private String name;
	private double emissionAmount;
	
	public CountryData(String name, double emissionAmount) {
		super();
		this.name = name;
		this.emissionAmount = emissionAmount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getEmissionAmount() {
		return emissionAmount;
	}

	public void setEmissionAmount(double emissionAmount) {
		this.emissionAmount = emissionAmount;
	}

}
