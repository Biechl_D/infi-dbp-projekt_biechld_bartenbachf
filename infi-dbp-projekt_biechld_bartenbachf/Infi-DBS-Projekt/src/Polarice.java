import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

public class Polarice{

	
	   public Polarice(){
	     
	   }

	   public void createDataset( ) throws ClassNotFoundException, SQLException {
		   
		   ArrayList<PolariceData> data = DBmanager.getInstance().PolariceNorthData();
		   
		   XYSeries datapoints = new XYSeries("Nordpol Eisumfang");
			for(int i = 0; i < data.size(); i++) {
				datapoints.add(data.get(i).getJahre(), data.get(i).getExtent());
			}
			
			XYSeriesCollection dataset = new XYSeriesCollection();
			dataset.addSeries(datapoints);

			XYSplineRenderer spline = new XYSplineRenderer(); //Anzeigen Punkte und Linie

			NumberAxis xax = new NumberAxis("Jahr");
			NumberAxis yax = new NumberAxis("Umfang");

			XYPlot plot = new XYPlot(dataset, xax, yax, spline);
			
			//Skalierung der Achsen - Start der Skalierung ab Parameter - X-Achse endet 
			xax.setLowerBound(data.get(0).getJahre()-10);
			yax.setLowerBound(12);
			xax.setUpperBound(data.get(data.size()-1).getJahre()+10);

			JFreeChart chart = new JFreeChart(plot);
			
			ChartPanel chartPanel = new ChartPanel(chart, false);
		   
			ApplicationFrame punkteframe = new ApplicationFrame("Nordpol-Eis"); //entspricht der Ueberschrift des Fensters
			
			punkteframe.setContentPane(chartPanel);
			punkteframe.pack();
			punkteframe.setVisible(true);
	
	   }
}
