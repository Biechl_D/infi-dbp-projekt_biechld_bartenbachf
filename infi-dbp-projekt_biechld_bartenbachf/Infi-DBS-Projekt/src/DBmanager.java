import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBmanager {

	static Connection con=null;
	private static DBmanager _instance;
	public static java.sql.Statement stmt=null;
	private static PreparedStatement stm=null;
	
	private DBmanager() throws ClassNotFoundException{
		Class.forName("com.mysql.cj.jdbc.Driver");
	}
	
	private Connection getConnection() throws SQLException{
		String url = "jdbc:mysql://localhost/projektdbp?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Berlin";
		//URL --> zeitzone wird auf Europa/Berlin gestellt da sonst Timezone Fehler und deaktiviert SSL(=verschlüsselte Verbindung zu MysqlServer)
		String user = "root";
		return DriverManager.getConnection(url, user, ""); 
	}
	
	public static DBmanager getInstance() throws ClassNotFoundException {
		if (_instance == null) {
			_instance = new DBmanager();
		}
		return _instance;
	}
	
	public ArrayList<PolariceData> PolariceNorthData() throws SQLException{
		
		con = getConnection();
		String selectquery = "Select year, extent from seaice where month = 01 AND day=01 and hemisphere='north'";
		
		ArrayList<PolariceData> polarData = new ArrayList<PolariceData>();
		
		try {	
			stm = con.prepareStatement(selectquery);
			ResultSet rs = stm.executeQuery(selectquery);
			//solange es ein nächstes Element in DB gibt wird while ausgeführt
			while (rs.next()) {
				polarData.add(new PolariceData(rs.getInt(1),rs.getDouble(2)));
			}
		}
			catch (SQLException e) {
	            e.printStackTrace();
	        } 
		return polarData;
	}
	
   public ArrayList<CountryData> CountryHighestEmissionData() throws SQLException{
		
		con = getConnection();
		String selectquery = "Select * from emissioncountry order by emissionAmountInMilliomMetricTons desc limit 10;";
		//desc in SQl dient dazu, dass es sortiert wird nach den hoechsten Werten
		
		ArrayList<CountryData> countryhighestData = new ArrayList<CountryData>();
		
		try {	
			stm = con.prepareStatement(selectquery);
			ResultSet rs = stm.executeQuery(selectquery);
			//solange es ein nächstes Element in DB gibt wird while ausgeführt
			while (rs.next()) {
				countryhighestData.add(new CountryData(rs.getString(1),rs.getDouble(2)));
			}
		}
			catch (SQLException e) {
	            e.printStackTrace();
	        } 
		return countryhighestData;
	}

   public ArrayList<CountryData> CountryLowestEmissionData() throws SQLException{
	
	con = getConnection();
	String selectquery = "Select * from emissioncountry order by emissionAmountInMilliomMetricTons limit 10;";
	
	ArrayList<CountryData> countryData = new ArrayList<CountryData>();
	
	try {	
		stm = con.prepareStatement(selectquery);
		ResultSet rs = stm.executeQuery(selectquery);
		//solange es ein nächstes Element in DB gibt wird while ausgeführt
		while (rs.next()) {
			countryData.add(new CountryData(rs.getString(1),rs.getDouble(2)));
		}
	}
		catch (SQLException e) {
            e.printStackTrace();
        } 
	return countryData;
   }
	
	public static void CSVreader() throws SQLException {
		
		String csvFile = "D:/5AHWII/Infi-DBP_Projekt/seaice.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

        	char quote = (char) 34;
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] seaice = line.split(cvsSplitBy);

     		   PreparedStatement ps = con.prepareStatement("insert into seaice(year, month, day, extent, hemisphere) values (?,?,?,?,?)");
     		   ps.setInt(1, Integer.parseInt(seaice[0].replace("\"","")));
     		   ps.setInt(2, Integer.parseInt(seaice[1].replace("\"","")));
     		   ps.setInt(3, Integer.parseInt(seaice[2].replace("\"","")));
     		   ps.setDouble(4, Double.parseDouble(seaice[3].replace("\"","")));
     		   ps.setString(5, seaice[6].replace("\"",""));
     		   ps.executeUpdate();
     		   
            }
            con.close();
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	public static void CSVreaderEmissionenCountries() throws SQLException {
		
		String csvFile = "D:/5AHWII/Infi-DBP_Projekt/CountryEmissionData.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

        	char quote = (char) 34;
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
               String[] emissionen = line.split(cvsSplitBy);
    			
     		   PreparedStatement ps = con.prepareStatement("insert into emissionCountry(country, emissionAmountInMilliomMetricTons) values (?,?)");
     		   ps.setString(1, emissionen[2]);
     		   ps.setDouble(2, Double.parseDouble(emissionen[11]));
     		   ps.executeUpdate();
     		   
            }
            con.close();
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	
  public static void CSVreaderEmission() throws SQLException {
		
		String csvFile = "D:/5AHWII/Infi-DBP_Projekt/global.1751_2014.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

        	char quote = (char) 34;
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
               String[] emissionen = line.split(cvsSplitBy);
    			
     		   PreparedStatement ps = con.prepareStatement("insert into CO2emissionen(year, amountInMillionMetricTons) values (?,?)");
     		   ps.setInt(1, Integer.parseInt(emissionen[0]));
     		   ps.setInt(2, Integer.parseInt(emissionen[1]));
     		   ps.executeUpdate();
     		   
            }
            con.close();
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
}
