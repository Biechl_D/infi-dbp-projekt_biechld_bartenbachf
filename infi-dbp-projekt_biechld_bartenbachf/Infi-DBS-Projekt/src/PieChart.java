import java.sql.SQLException;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.ApplicationFrame;

public class PieChart {

	public PieChart() {}
	
	public void createDataset( ) throws ClassNotFoundException, SQLException{
		
		 ArrayList<CountryData> data = DBmanager.getInstance().CountryHighestEmissionData();
		
		 DefaultPieDataset dataset = new DefaultPieDataset( );     
	     for(int i = 0; i < data.size(); i++) {
	    	  dataset.setValue(data.get(i).getName() , data.get(i).getEmissionAmount()); 
		 }
	     
	     JFreeChart chart = ChartFactory.createPieChart(      
	             "Top 10 Emissionslaender",   // chart title 
	             dataset,          // data    
	             false,             // exclude legend   
	             true, 
	             true);  //wenn Maus auf St�ck ist -- Menge anzeigen
	       
	     ChartPanel chartPanel = new ChartPanel(chart, false);
		   
		 ApplicationFrame chartframe = new ApplicationFrame("Nordpol-Eis"); //entspricht der Ueberschrift des Fensters
			
		 chartframe.setContentPane(chartPanel);
		 chartframe.pack();
		 chartframe.setVisible(true);
	}
}
